+++
title = "Experience"
date = "2020-03-24"
sidemenu = "true"
description = "Experience"
+++  

## Professional Experience

* **Research Associate**
    - NCSR Demokritos (June 2019 - today)
        1. DARE project
        2. Fair4Fusion
* **Software Developer**
    - Voiceweb S.A. (August 2018 - August 2019)
        - Speech recognition design, development and implementation with Ravenclaw technique. 
        - Significant contributions, improvements and extensions of company products (Dialog manager and NLI platform). 
        - Technologies used included Java 8, Spring Framework, Spring MVC, REST and SOAP web services and others.
    - G.N.T. Information Systems S.A. (May 2016 - July 2018)
        - Java EE / Spring (EJB3, JPA, Spring Framework, Spring MVC)
        - RESTful and SOAP web services
        - Test-driven development (JUnit, Arquillian, Spring Integration Tests)
        - Front-end development: Vaadin, Android
        - various: maven, gradle, git, svn, cvs, GitLab CI/CD
* **Consultant**
    - Deloitte S.A. (February 2014 - May 2016)
        - Risk Analysis
        - SAP FI
        - Excel VBA

## Internships

* **HR Manager Assistant**
    - Attica (October 2012 - January 2013)
        1. CVs screening & candidate's identification
        2. Interviews
        3. Labor data control and analysis
* **Communication Department** 
    - Back of Greece (June 2011 - December 2011)
        1. Daily reports for the Bank of Greece Governor and the Board of Directors
        2. Event organizations
        3. Communication with reporters
        4. Proof-read Press Releases