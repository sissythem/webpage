+++
title = "Education"
date = "2020-03-24"
sidemenu = "true"
description = "Education"
+++

### Academic Qualifications

* PhD in Argument Mining (2021-today)
* M.Sc. in Data Science and Information Technologies (GPA: 8.5), Department of Informatics and Telecommunications of the University of Athens (2016-2018)
    * M.Sc. Thesis: ’A study of text representations for Hate Speech Detection’, implemented both in Java and Python. Used several features (Bag of words, n-grams & character n-grams, word2vec, sentiment and syntax analysis, n-gram graphs) and classification algorithms (Naive Bayes, Logistic Regression, Random Forest, KNN, Neural Networks) for this task. Supervisors: Professor Panagiotis Stamatopoulos (UoA) & Dr. George Giannakopoulos (Researcher at NCSR Demokritos)
* B.Sc. in Management Science and Technology (GPA: 7.0), Athens University of Economics and Business (2008-2013)
* High Scool, Lycee Franko-Hellenique Eugene Delacroix (2002-2008)

### Seminars

* Artificial Intelligence for Multilanguage Services for Citizens and Businesses, ELRC workshop (virtual event, 15/12/2020)
* Using text data to improve causal analyses of electronic health records (speaker: Dr. Aaron Kaufman, place: NCSR Demokritos, date: 7/11/2019)
* 1st Athens NLP Summer School (place: NCSR Demokritos, dates: 18-25/09/2019)
* 20th International Conference on Computational Linguistics and Intelligent Text Processing (CICLing2019, place: LaRochelle France, dates: 7-13/04/2019)
* Object-Centric Machine Learning (speaker: Leonidas Guibas, place: Department of Informatics and Telecommunications, date: 5/5/2017)
* The role of relays in the 5G systems (speaker: Vougioukas Dimosthenis, place: Department of Informatics and Telecommunications, date: 26/5/2017)
* Hardware architectures for telecommunication vertices (speaker: Vlassopoulos Nikolaos, place: Department of Informatics and Telecommunications, date: 26/5/2017)
* Human-Centered Entrepreneurship (9th Student Conference of Department of Management Science and Technology - DMST, date: 10/5/2012)
* Entrepreneurship, openness and new technologies (8th Student Conference of DMST, date: 12/5/2011)
* Youth Entrepreneurship and digital environment (7th Student Conference of DMST, date 13/5/2010)
* Management science and technology in the modern business environment (6th Student Conference of DMST, date: 7/5/2009)
* Management science, technology and green economy (5th Student Conference of DMST, date: 8/5/2008)
