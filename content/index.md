+++
title = "Sissy Themeli"
date = "2020-03-24"
sidemenu = "true"
description = "About me"
+++ 

I am a Ph.D. Candidate at the Department of Informatics and Telecommunications (DIT) at the University of Athens (UoA), studying deep machine learning methods for mining arguments from natural language resources. I hold an M.Sc. in Computer Science from DIT/UoA and a B.Sc. in Management Science and Technology from the Department of Management Science and Technology at the Athens University of Economics and Business. My M.Sc. thesis involved the studying of various text representations and machine learning algorithms to detect Hate Speech in online content.

I have also demonstrated experience as a Backend Software Engineer building web services and RESTful APIs as well as dialog management systems for Omni-CHannel Natural Language Interactions. Currently, I am working as a Research Associate at the SKEL Lab of IIT at NCSR Demokritos, participating in EU and national funded projects in the areas of Machine Learning and Natural Language Processing.

You can get a pdf of my [detailed](/pdfs/cv.pdf) and [short](/pdfs/cv_short.pdf) cv in English.

## Personal Info

* Birth date and place: 05/08/1990 | Marousi, Athens
* Marital Status: Single
* Email: skthemeli@gmail.com

## Computer Skills

* **Programming Languages:** Python, Java, R, C/C++
* **Frameworks:** Django, Flask, Spring, JavaEE (EJB3), Vaadin, Android
* **ML & Data Science libraries:** PyTorch, Tensorflow/Keras, scikit-learn, transformers, flairNLP, spaCy, numpy, pandas, scipy, Weka
* **Testing:** JUnit, Arquillian, Spring Integration Tests
* **Web services:** RESTful, SOAP
* **Databases & Search Engines:** MySQL, Oracle SQL, MS SQL Server, PostgreSQL, MongoDB, Elasticsearch, Solr
* **Web:** HTML, CSS, SCSS, XML, JSON 
* **Semantic Web:** RDF, (geo/st) SPARQL, OWL
* **Deployment:** Kubernetes, Docker, JBoss/Wildfly, Tomcat/TomEE
* **CI/CD:** GitLab CI/CD, Jenkins
* **Build Tools:** Maven, Gradle
* **Version Control:** Git, SVN, CVS
* **IDEs:** IntelliJ IDEA, Pycharm, Eclipse, RStudio, Android Studio
* **OS:** Linux, Windows
* **Other:** LaTeX, MS Office

## Interests

* Volleyball player since the age of 8 (winner of the national championship with Iraklis Kifisias volleyball team in July 2004)
* Technology, Theatre, Cinema, Reading
